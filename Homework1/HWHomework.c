//
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
#include "stdlib.h"
#include <math.h>
#include <time.h>
#include <string.h>

/*
 * Задание 1. (1 балл)
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:
 *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
long** createMatrix(const long rows, const long columns) {
    long** matrix;
    matrix = malloc(rows * sizeof(long*));	
    for (int i = 0; i < rows; i++) {
        matrix[i] = malloc(columns * sizeof(long));
    }
    srand(time(0));
    rand();
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            matrix[i][j] = rand() % 201 - 100;
        }
    }
    return matrix;
}

/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности (1 балл):
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности (2 балла):
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
char* stringForNumberString(const char *numberString) {
    int myStringLen = 1;
    char* myString = malloc(sizeof(char));
    myString[0] = 0;
    unsigned long size = strlen(numberString);
    for (int i = 0; i < size; ++i)
    {
        switch (numberString[i])
        {
            case '1':
                myString = realloc(myString, myStringLen += 4);
                strcat(myString, " one");
                break;
            case '2':
                myString = realloc(myString, myStringLen += 4);
                strcat(myString, " two");
                break;
            case '3':myString = realloc(myString, myStringLen += 6);
                strcat(myString, " three");
                break;
            case '4':
                myString = realloc(myString, myStringLen += 5);
                strcat(myString, " four");
                break;
            case '5':
                myString = realloc(myString, myStringLen += 5);
                strcat(myString, " five");
                break;
            case '6':
                myString = realloc(myString, myStringLen += 4);
                strcat(myString, " six");
                break;
            case '7':
                myString = realloc(myString, myStringLen += 6);
                strcat(myString, " seven");
                break;
            case '8':
                myString = realloc(myString, myStringLen += 6);
                strcat(myString, " eight");
                break;
            case '9':
                myString = realloc(myString, myStringLen += 5);
                strcat(myString, " nine");
                break;
            case '0':
                myString = realloc(myString, myStringLen += 5);
                strcat(myString, " zero");
                break;
            case '.':
                myString = realloc(myString, myStringLen += 4);
                strcat(myString, " dot");
                break;
            default:
                myString = realloc(myString, myStringLen += 21);
                strcat(myString, " NOT A DIGIT OR A DOT");
        }
    }
    return myString;
}


/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 // *
 // * Возвращаемое значение:
 // *      Структура HWPoint, содержащая x и y координаты точки;
 // *
 // * Особенность:
 // *      Начало координат находится в левом верхнем углу.
 // *
 // * Обычный уровень сложности (2 балла):
 // *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 // *
 // * Высокий уровень сложности (3 балла):
 // *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 // *
 // * PS: Не обязательно, чтобы ваша анимация один в один совпадала с примером.
 // * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 // */
HWPoint pointForAnimationTime(const double time, const double canvasSize) {
    //анимация графика косинуса
    double  localtime = fmod(time * 15, canvasSize);
    return (HWPoint){localtime, (cos(localtime / 15) * (canvasSize / 4)) + canvasSize / 2};
}

